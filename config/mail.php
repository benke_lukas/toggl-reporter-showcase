<?php
/*return array (
    "driver"   => "smtp",
    "host"     => "mailtrap.io",
    "port"     => 2525,
    "username" => "9d8d3693f85997",
    "password" => "49d67ab13d596a",
    "sendmail" => "/usr/sbin/sendmail -bs",
    "pretend"  => false
);*/

return array (
    "driver"   => env('MAIL_DRIVER'),
    "host"     => env('MAIL_HOST'),
    "port"     => env('MAIL_PORT'),
    "username" => env('MAIL_USERNAME'),
    "password" => env('MAIL_PASSWORD'),
    "pretend"  => false,
    "encryption" => '',
);