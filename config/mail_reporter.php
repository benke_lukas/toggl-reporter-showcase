<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 10:40
 */
return [
    'default_from' => [
        'email' => 'mail@domena.cz',
        'name' => 'NAME'
    ],
    'default_to' => [
        'mail1@domena.cz',
        'mail2@domena.cz'
    ],
    'daily' => [
        'default_subject' => 'Hodiny - %s'
    ],
    'direct' => [
        'default_subject' => 'Hodiny - %s pro %s'
    ]
];