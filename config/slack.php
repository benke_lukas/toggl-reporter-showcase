<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 30.05.2016
 * Time: 18:05
 */
return [
    'system-toggl-reporter' => [
        'webhook_url' => 'HOOK_URL'
    ],
    'users' => [
        //slack API ID => local name
        'SOME_ID' => 'SOME_LOCAL_NAME'
    ],
    //slack commands config
    'commands' => [
        '/postpone-daily-report' => [
            'token' => 'SOME_TOKEN'
        ]
    ]
];