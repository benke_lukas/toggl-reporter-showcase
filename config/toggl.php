<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 14.04.2016
 * Time: 13:17
 */
return [
    'users' => [
        'user_1'  => [
            'api_key'      => 'SOME_API_KEY',
            'workspace_id' => 000000,
            'user_agent'   => 'some@email.com',
            'tags' => null,
        ],
        'user_2'  => [
            'api_key'      => 'SOME_API_KEY',
            'workspace_id' => 111111,
            'user_agent'   => 'some_second@email.com',
            'tags' => null,
        ],
    ]
];