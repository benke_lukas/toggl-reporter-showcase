<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 30.05.2016
 * Time: 18:10
 */

namespace App\Events;


class TogglReportSent extends Event
{

    /**
     * @var
     */
    public $type;
    /**
     * @var null
     */
    public $user;
    /**
     * @var null
     */
    public $excel_file_path;
    /**
     * @var
     */
    public $date_from;
    /**
     * @var
     */
    public $date_to;

    /**
     * TogglReportSent constructor.
     * @param string $type
     * @param $date_from
     * @param $date_to
     * @param null string $user
     * @param null $excel_file_path
     */
    public function __construct($type, $date_from, $date_to, $user = null, $excel_file_path = null)
    {

        $this->type            = $type;
        $this->user            = $user;
        $this->excel_file_path = $excel_file_path;
        $this->date_from = $date_from;
        $this->date_to = $date_to;
    }
}