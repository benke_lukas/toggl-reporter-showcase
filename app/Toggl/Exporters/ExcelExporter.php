<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 29.02.2016
 * Time: 12:22
 */

namespace App\Toggl\Exporters;

use Excel;
use App\Toggl\Exporters\Contracts\Exporter;

class ExcelExporter extends Exporter
{

    public function exportSummary($data, $user)
    {
        $file = Excel::create("hodiny-$user-" . time(), function ($excel) use ($data) {
            $excel->sheet('1', function ($sheet) use ($data) {
                $row  = 1;
                foreach ($data['data'] as $index => $project) {
                    $sheet->appendRow([
                        $project['title']['project'] . ' - ' . $project['title']['client'],
                        $this->extractTimeFromMilliseconds($project['time'])
                    ]);
                    $sheet->cell('A'.$row, function($cell){
                        $cell->setFontWeight('bold');
                    });
                    $sheet->cell('B'.$row, function($cell){
                        $cell->setFontWeight('bold');
                    });
                    $row++;

                    foreach ($project['items'] as $item) {
                        $sheet->appendRow([
                            $item['title']['time_entry']
                        ]);
                        $sheet->cell('A'.$row, function($cell){
                            $cell->setAlignment('right');
                        });
                        $row++;
                    }
                }
            });
        });
        $file = $file->store('xlsx', public_path('storage/reports'), true);

        return $file;
    }
}