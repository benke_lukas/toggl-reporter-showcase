<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 10:31
 */

namespace App\Toggl\Exporters\Contracts;


abstract class Exporter
{
    abstract public function exportSummary($data, $user);

    protected function extractTimeFromMilliseconds($time)
    {
        $uSec = $time % 1000;
        $time = floor($time / 1000);

        $seconds = $time % 60;
        $time    = floor($time / 60);

        $minutes = $time % 60;
        $time    = floor($time / 60);

        $hours = $time % 60;
        $time  = floor($time % 60);

        return "$hours:$minutes:$seconds";
    }
}