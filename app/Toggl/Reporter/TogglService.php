<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 29.02.2016
 * Time: 10:40
 */

namespace App\Toggl\Reporter;

use AJT\Toggl\ReportsClient;
use AJT\Toggl\TogglClient;
use Carbon\Carbon;

class TogglService
{

    private $workspace_id;
    private $user_agent;
    private $api_key;

    /**
     * TogglService constructor.
     * @param $api_key
     * @param $workspace_id
     * @param null $user_agent
     */
    public function __construct($api_key, $workspace_id = null, $user_agent = null)
    {
        $this->reportsClient = ReportsClient::factory([
            'api_key' => $api_key,
            'debug'   => env('APP_DEBUG')
        ]);
        $this->togglClient   = TogglClient::factory([
            'api_key' => $api_key,
            'debug'   => env('APP_DEBUG')
        ]);
        $this->api_key       = $api_key;
        $this->workspace_id  = $workspace_id != null ? $workspace_id : env('TOGGL_WORKSPACE_ID');
        $this->user_agent    = $user_agent != null ? $user_agent : env('TOGGL_USER_AGENT');
    }

    public function getWorkspaces()
    {
        $workspaces = $this->togglClient->getWorkspaces([]);
        print_r($workspaces);
    }

    /**
     * Gets report for given datetime range
     *
     * @param Carbon $dateFrom
     * @param Carbon $dateTo
     * @param null $grouping
     * @param null $subgrouping
     * @param null $tags
     * @return
     */
    public function summaryReport(
        Carbon $dateFrom,
        Carbon $dateTo,
        $grouping = null,
        $subgrouping = null,
        $tags = null
    ) {
        $data = $this->reportsClient->summary([
            'workspace_id' => $this->workspace_id,
            'user_agent'   => $this->user_agent,
            'since'        => $dateFrom->toIso8601String(),
            'until'        => $dateTo->toIso8601String(),
            'grouping'     => $grouping == null ? 'projects' : $grouping,
            'tag_ids'      => $tags != null ? implode(',', $tags) : 0,
            'subgrouping'  => $subgrouping == null ? 'time_entries' : $grouping,
        ]);

        return $data;
    }
}