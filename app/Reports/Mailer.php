<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 10:38
 */

namespace App\Reports;


use App\Events\TogglReportSent;
use Carbon\Carbon;
use Event;
use Mail;

class Mailer
{

    private $config;
    /**
     * Mailer constructor.
     */
    public function __construct()
    {
        $this->config = config('mail_reporter');
    }

    public function sendDaily(Carbon $date_from, array $xlsx, $subject = null, $from = null, $to = null) {
        if ( $subject === null ) {
            $subject = sprintf($this->config['daily']['default_subject'], $date_from->format('j.n.Y'));
        }
        if ( $from === null ) {
            $from = $this->config['default_from']['email'];
            $from_name = $this->config['default_from']['name'];
        }
        if ( $to === null ) {
            $to = $this->config['default_to'];
        }

        $callback = function($message) use($date_from, $xlsx, $subject, $from, $from_name, $to) {
            $message->subject($subject);
            $message->from($from, $from_name);
            $message->to((array)$to);

            foreach ($xlsx as $report) {
                $message->attach($report['full'], ['as' => $report['file']]);
            }
        };

        return $this->send('mails.reports.daily', $callback);
    }

    public function sendDirect($userName, $date, $report, $subject = null, $from = null, $to = null) {
        if ( $subject === null ) {
            $subject = sprintf($this->config['direct']['default_subject'], $date->format('j.n.Y'), $userName);
        }
        if ( $from === null ) {
            $from = $this->config['default_from']['email'];
            $from_name = $this->config['default_from']['name'];
        }
        if ( $to === null ) {
            $to = $this->config['default_to'];
        }

        $callback = function($message) use($userName, $date, $report, $subject, $from, $from_name, $to) {
            $message->subject($subject);
            $message->from($from, $from_name);
            $message->to((array)$to);

            $message->attach($report['full'], ['as' => $report['file']]);
        };

        return $this->send('mails.reports.day', $callback);
    }

    protected function send($view, \Closure $callback, $params = []) {
        $mail = \Mail::send($view, $params, $callback);

        return $mail;
    }
}