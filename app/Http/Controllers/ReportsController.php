<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 10:20
 */

namespace App\Http\Controllers;


use App\Events\TogglReportSent;
use App\Reports\Mailer;
use App\Toggl\Exporters\ExcelExporter;
use App\Toggl\Reporter\TogglService;
use Event;
use Laravel\Lumen\Routing\Controller;
use App\Toggl\Exporters\Contracts\Exporter;

class ReportsController extends Controller
{

    /**
     * @var Exporter
     */
    private $exporter;
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * ReportsController constructor.
     * @param Exporter $exporter
     * @param Mailer $mailer
     */
    public function __construct(Exporter $exporter, Mailer $mailer)
    {
        $this->exporter = $exporter;
        $this->mailer = $mailer;
    }

    public function daily() {
        $users = config('toggl.users', []);
        foreach ($users as $name => $user) {
            $togglService = new TogglService(
                $user['api_key'],
                $user['workspace_id'],
                $user['user_agent']
            );
            $from         = \Carbon\Carbon::now();
            $to           = \Carbon\Carbon::now();

            $data = $togglService->summaryReport($from, $to, null, null, $user['tags']);

            $xlsx[] = $this->exporter->exportSummary($data, $name);

            $togglService = null;
        }
        $this->mailer->sendDaily($from, $xlsx);

        $urls = [];
        foreach ( $xlsx as $report ) {
            $urls[] = url("storage/{$report['file']}");
        }
        $event = new TogglReportSent(
            'daily',
            $from,
            $to,
            null,
            $urls
        );
        Event::fire($event);
    }

    public function direct($user, $date) {
        $userName = $user;

        $user     = array_get(config('toggl.users'), $userName, null);
        if ($user === null) {
            return response("Invalid user", 500);
        }

        $togglService = new TogglService(
            $user['api_key'],
            $user['workspace_id'],
            $user['user_agent']
        );
        $from         = \Carbon\Carbon::parse($date);
        $to           = \Carbon\Carbon::parse($date);

        $data = $togglService->summaryReport($from, $to, null, null, $user['tags']);

        $report = $this->exporter->exportSummary($data, $userName);

        $this->mailer->sendDirect($userName, $from);

        $event = new TogglReportSent(
            'direct',
            $from,
            $to,
            $userName,
            url("storage/{$report['file']}")
        );
        Event::fire($event);
    }
}