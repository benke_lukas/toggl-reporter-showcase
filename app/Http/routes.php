<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$app->get('/daily', ['as' => 'toggl.report.daily', 'uses' => 'ReportsController@daily']);

$app->get('/users/{user}/day/{date}', ['as' => 'toggl.report.direct', 'uses' => 'ReportsController@direct']);

$app->post('/slack/postpone-daily-report', function(){
    //TODO: implement slack command to postpone daily report for given user
});