<?php

namespace App\Providers;

use App\Events\TogglReportSent;
use App\Listeners\ReportSentSlackMessager;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        TogglReportSent::class => [
            ReportSentSlackMessager::class,
        ],
    ];
}
