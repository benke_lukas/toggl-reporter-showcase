<?php

namespace App\Providers;

use App\Toggl\Exporters\ExcelExporter;
use Illuminate\Support\ServiceProvider;
use App\Toggl\Exporters\Contracts\Exporter;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            Exporter::class,
            ExcelExporter::class
        );
    }
}
