<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 30.05.2016
 * Time: 18:14
 */

namespace App\Listeners;


use App\Events\TogglReportSent;
use App\Slack\Webhooks\TogglReporter;

class ReportSentSlackMessager
{

    /**
     * @var TogglReporter
     */
    private $slackReporter;

    /**
     * ReportSentSlackMessager constructor.
     * @param TogglReporter $slackReporter
     */
    public function __construct(
        TogglReporter $slackReporter
    ) {
        $this->slackReporter = $slackReporter;
    }

    public function handle(TogglReportSent $event)
    {
        switch ($event->type) {
            case 'direct': {
                $response = $this->slackReporter->sendDirectReport(
                    $event->user,
                    $event->date_from,
                    $event->date_to,
                    $event->excel_file_path
                );
            }
                break;
            case 'daily': {
                $response = $this->slackReporter->sendDailyReport(
                    $event->date_from,
                    $event->date_to,
                    $event->excel_file_path
                );
            }
        }
    }
}