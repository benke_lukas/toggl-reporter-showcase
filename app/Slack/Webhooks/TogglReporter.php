<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 30.05.2016
 * Time: 18:17
 */

namespace App\Slack\Webhooks;


use App\Slack\WebhookClient;

class TogglReporter extends WebhookClient
{

    public function sendDirectReport($user, $date_from, $date_to, $excel_file)
    {
        return $this->message(
            "Report for user $user for dates: $date_from to $date_to was generated. <$excel_file>",
            null
        )->to(config('slack.system-toggl-reporter.webhook_url'))->send();
    }

    public function sendDailyReport($date_from, $date_to, $excel_file)
    {
        $links = "";
        if ( is_array($excel_file) ) {
            foreach ( $excel_file as $file ) {
                $links .= "<$file>, ";
            }
        }
        return $this->message(
            "Report for all users for date: $date_from was sent, excel files: $links",
            null
        )->to(config('slack.system-toggl-reporter.webhook_url'))->send();
    }
}