<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 31.05.2016
 * Time: 15:08
 */

namespace App\Slack\Commands;


use App\Slack\Commands\Exceptions\CommandConfigNotFound;

class BaseSlackCommand
{
    protected $command_name;

    protected function getConfig() {
        $config = config('slack');
        $command_config = array_get($config, "commands.$this->command_name", false);

        if (!$command_config) throw new CommandConfigNotFound;

        return $command_config;
    }
}