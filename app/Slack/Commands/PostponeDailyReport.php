<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 31.05.2016
 * Time: 14:58
 */

namespace App\Slack\Commands;

use Request;

class PostponeDailyReport extends BaseSlackCommand
{

    public function execute()
    {
        //fetch data
        $token   = Request::get('token', null);
        $user_id = Request::get('user_id', null);
        $text    = Request::get('text', null);

        //fetch user
        $user = $this->fetchUser($user_id);
        //fetch config
        
    }

    private function fetchUser($user_id)
    {
        $user = array_get(config('slack.users'), $user_id, false);
        if (!$user) throw new \Exception("Cannot find local user for slack ID $user_id");

        return $user;
    }
}