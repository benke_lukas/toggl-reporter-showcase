<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 30.05.2016
 * Time: 18:17
 */

namespace App\Slack;


use Guzzle\Http\Exception\ClientErrorResponseException;
use Guzzle\Http\Exception\ServerErrorResponseException;

abstract class WebhookClient extends BaseClient
{

    protected $channel_url;
    protected $data;


    public function message($message, $channel_override = null)
    {
        $this->data = [
            'text' => $message
        ];
        if ( $channel_override !== null ) {
            $this->data['channel'] = $channel_override;
        }

        return $this;
    }

    public function to($channel_url)
    {
        $this->channel_url = $channel_url;

        return $this;
    }

    public function send()
    {
        try {
            $response = $this->client->post(
                $this->channel_url,
                null,
                $this->dataAsJson()
            )->send();

            return $response;
        } catch (ServerErrorResponseException $e) {
            echo($e->getResponse()->getBody(true));
            throw $e;
        }
    }

    private function dataAsJson()
    {
        if (is_array($this->data)) {
            $to_decode = $this->data;
        } else {
            $to_decode = [];
        }

        return json_encode($to_decode);
    }
}