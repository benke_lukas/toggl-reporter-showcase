<?php
/**
 * Created by PhpStorm.
 * User: Lukas
 * Date: 30.05.2016
 * Time: 18:15
 */

namespace App\Slack;


use Guzzle\Http\Client;

class BaseClient
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * BaseClient constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}