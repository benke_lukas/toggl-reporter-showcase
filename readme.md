# Toggl Reporter

Application for pulling data about work hours from toggl.com, exporting them to excel, and sending them to backoffice for postprocess.

Based on the Laravel Lumen framework.

## Installation

1. Clone repository
2. Run ```composer install```
3. Setup config in .env file (take a look at .env.example)
4. Setup config in ```app/config/slack.php```, ```app/config/toggl.php``` and ```app/config/mail_reporter.php```
